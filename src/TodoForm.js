import {useState} from 'react';
function TodoForm({addTodo}) {
    const [text, setText] = useState('')
    const handleChange = (e) => {
        setText(e.currentTarget.value)        
    }

    const handleSubmit = (e) => {
        e.preventDefault("")
        addTodo(text)
    }

    return (
        <div>
            <input
                type="text"
                text={text}
                onChange={handleChange}                
            />
            <button onClick={handleSubmit}>Добавить</button>
        </div>
    )
}

export default TodoForm