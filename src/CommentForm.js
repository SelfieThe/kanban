import {useState} from 'react';
function CommentForm({addComment}) {
    const [text, setText] = useState('')
    const handleChange = (e) => {
        setText(e.currentTarget.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        addComment(text)       
    }

    return (
        <div>
            <textarea 
                rows="4" 
                cols="65" 
                name="comment"                 
                text={text}
                onChange={handleChange}>
                Enter text here...
            </textarea>
            <button onClick={handleSubmit}>Добавить</button>
        </div>
    )
}

export default CommentForm