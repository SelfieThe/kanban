import { useEffect, useCallback, useState } from 'react';
import CommentForm from './CommentForm'
import Comment from './Comment'

function CardForm(popup) {
    // console.log(popup)
    const [comment, setComment] = useState([])
    const addComment = (text) => {
        if(text) {
          const newComment = {
            id: popup.id,
            comment: text,  
            complete: false
          }
          setComment([...comment,newComment])
          localStorage.setItem('comments', JSON.stringify([...comment,newComment]));        
        }
    }
    const removeComment = (id) => {
        setComment([...comment.filter((comment) => comment.id !== id)])
    localStorage.setItem('comments', JSON.stringify([...comment.filter((comment) => comment.id !== id)]));
    }
    const handleToggle = (id) => {
        setComment([...comment.map((comment) => comment.id === id ? {...comment, complete : !comment.complete} : {...comment})])
    }
    // console.log(popup.status)
    const closePopup = (status) => { 
        popup.setVisible(status)  
    }
    const escFunction = useCallback((event) => {
        if(event.keyCode === 27) popup.setVisible("hidden")
      });
    
    useEffect(() => {
        document.addEventListener("keydown", escFunction, false);  
        if (localStorage.getItem('comments') !== null) {
            let comments = JSON.parse(localStorage.getItem('comments'));
            setComment(comments)
        }        
    }, comment)
    
    return (
        <div id="CardForm" className="popup" style={{visibility: popup.status}}>
            <div>
                <a onClick={() => closePopup("hidden")} className="closeBtn">X</a>    
                <div className="popupTodoName">
                    <p>TodoName: {popup.name}</p>
                </div>
                <p>Categories: {popup.categories}</p>
                <p>Author: {localStorage.getItem('person')}</p>                 
                <div>
                    <p>Description:                         
                    <CommentForm addComment={addComment}/>
                    {comment.map((item) => {
                        return (
                            <Comment
                            comment={item}
                            key={item.id}
                            toggleComment={handleToggle}
                            removeComment={removeComment}
                            />
                        )
                    })}
                    </p>
                </div>
            </div>
        </div>
    )
}

export default CardForm