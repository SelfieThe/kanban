import {useState, useEffect} from 'react';
import './App.css';
import Todo from './Todo'
import TodoForm from './TodoForm'


function App() {    
  window.onload = function() {
    if (localStorage.getItem('person') === null) {
      let person = prompt("Как Вас зовут?")
      localStorage.setItem('person', person);
    }
  }
  const [todo, setTodo] = useState([])
  const [todoInProgress, setTodoInProgress] = useState([])
  const [todoTesting, setTodoTesting] = useState([])
  const [todoDone, setTodoDone] = useState([])
  const [titleTodo, setTitleTodo] = useState("Todo")
  const [titleInProgress, setTitleInProgress] = useState("In progress")
  const [titleTesting, setTitleTesting] = useState("Testing")
  const [titleDone, setTitleDone] = useState("Done")
  
  const addTodo = (text) => {        
    if(text) {
      const newTodo = {
        id: Math.random(),
        todo: text,  
        complete: false
      }
      setTodo([...todo,newTodo])          
      localStorage.setItem('todos', JSON.stringify([...todo,newTodo]));          
    }
  }

  const addTodoInProgress = (text) => {        
    if(text) {
      const newTodoInProgress = {
        id: Math.random(),
        todo: text,  
        complete: false
      }
      setTodoInProgress([...todoInProgress,newTodoInProgress])          
      localStorage.setItem('todosInProgress', JSON.stringify([...todoInProgress,newTodoInProgress]));          
    }
  }

  const addTodoTesting = (text) => {        
    if(text) {
      const newTodoTesting = {
        id: Math.random(),
        todo: text,  
        complete: false
      }
      setTodoTesting([...todoTesting,newTodoTesting])          
      localStorage.setItem('todosTesting', JSON.stringify([...todoTesting,newTodoTesting]));          
    }
  }

  const addTodoDone = (text) => {        
    if(text) {
      const newTodoDone = {
        id: Math.random(),
        todo: text,  
        complete: false
      }
      setTodoDone([...todoDone,newTodoDone])          
      localStorage.setItem('todosDone', JSON.stringify([...todoDone,newTodoDone]));          
    }
  }

  const removeTodo = (id) => {
    setTodo([...todo.filter((todo) => todo.id !== id)])
    localStorage.setItem('todos', JSON.stringify([...todo.filter((todo) => todo.id !== id)]));

    setTodoInProgress([...todoInProgress.filter((todoInProgress) => todoInProgress.id !== id)])
    localStorage.setItem('todosInProgress', JSON.stringify([...todoInProgress.filter((todoInProgress) => todoInProgress.id !== id)]));
    
    setTodoTesting([...todoTesting.filter((todoTesting) => todoTesting.id !== id)])
    localStorage.setItem('todosTesting', JSON.stringify([...todoTesting.filter((todoTesting) => todoTesting.id !== id)]));
    
    setTodoDone([...todoDone.filter((todoDone) => todoDone.id !== id)])
    localStorage.setItem('todosDone', JSON.stringify([...todoDone.filter((todoDone) => todoDone.id !== id)]));  
  }
  const handleToggle = (id) => {    
    setTodo([...todo.map((todo) => todo.id === id ? {...todo, complete : !todo.complete} : {...todo})])
  }

  const handleChangeTitleTodo = (text) => {    
    setTitleTodo(text.target.value)
  }
  const handleChangeTitleInProgress = (text) => {    
    setTitleInProgress(text.target.value)
  }
  const handleChangeTitleTesting = (text) => {    
    setTitleTesting(text.target.value)
  }
  const handleChangeTitleDone = (text) => {    
    setTitleDone(text.target.value)
  }    
  
  useEffect(() => {        
    if (localStorage.getItem('todos') !== null){
      let todos = JSON.parse(localStorage.getItem('todos'));
      setTodo(todos)          
    }

    if (localStorage.getItem('todosInProgress') !== null){
      let todosInProgress = JSON.parse(localStorage.getItem('todosInProgress'));
      setTodoInProgress(todosInProgress)          
    }

    if (localStorage.getItem('todosTesting') !== null){
      let todosTesting = JSON.parse(localStorage.getItem('todosTesting'));
      setTodoTesting(todosTesting)          
    }
    
    if (localStorage.getItem('todosDone') !== null){
      let todosDone = JSON.parse(localStorage.getItem('todosDone'));
      setTodoDone(todosDone)          
    }
  }, todo, todoInProgress, todoTesting, todoDone)
  
  return (
    <div className="container">
      <div className="card1">
      <div className="title">
        <input type="text" value={titleTodo} onChange={handleChangeTitleTodo}/>
      </div>
      <TodoForm addTodo={addTodo}/>
      {todo.map((item) => {
        return (
          <Todo            
          todo={item}
          key={item.id}
          toggleTodo={handleToggle}
          removeTodo={removeTodo}
          categories={titleTodo}             
          />
        )
      })}
      </div> 
      <div className="card2">
        <div className="title">
          <input type="text" id="fname" name="fname" value={titleInProgress} onChange={handleChangeTitleInProgress} />
        </div>
        <TodoForm addTodo={addTodoInProgress}/>
        {todoInProgress.map((item) => {
          return (
            <Todo            
            todo={item}
            key={item.id}
            toggleTodo={handleToggle}
            removeTodo={removeTodo}
            categories={titleInProgress}             
            />
          )
        })}
      </div>
      <div className="card3">
        <div className="title">
          <input type="text" id="fname" name="fname" value={titleTesting} onChange={handleChangeTitleTesting} />
        </div>
        <TodoForm addTodo={addTodoTesting}/>
        {todoTesting.map((item) => {
          return (
            <Todo            
            todo={item}
            key={item.id}
            toggleTodo={handleToggle}
            removeTodo={removeTodo}
            categories={titleTesting}             
            />
          )
        })}
      </div>
      <div className="card4">
        <div className="title">
          <input type="text" id="fname" name="fname" value={titleDone} onChange={handleChangeTitleDone} />
        </div>
        <TodoForm addTodo={addTodoDone}/>
        {todoDone.map((item) => {
          return (
            <Todo            
            todo={item}
            key={item.id}
            toggleTodo={handleToggle}
            removeTodo={removeTodo}
            categories={titleDone}             
            />
          )
        })}
      </div>      
    </div>
  );
}

export default App