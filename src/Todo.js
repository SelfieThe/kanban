import './App.css'
import CardForm from './CardForm'
import {useState} from 'react';

function Todo({todo, toggleTodo, removeTodo, categories}) {
    const [visible, setVisible] = useState("hidden")
    // console.log(categories)
    const showPopup = () => {          
        if (visible === "hidden") 
            setVisible("visible")
        if (visible === "visible") 
        setVisible("hidden")        
    }
    
    return (
        <div key={todo.id} className="list-item">
            
            <div id="nameTodo" onClick={() => toggleTodo(todo.id)}
                onClick={() => showPopup()}>
                {todo.todo}                                
            </div>
            <div onClick={() => removeTodo(todo.id)} className="delBtn">
                Удалить
            </div>
            <div>            
                <CardForm status={visible} setVisible={setVisible} name={todo.todo} id={todo.id} categories={categories}/>
            </div>  
            
        </div>
    )
}

export default Todo