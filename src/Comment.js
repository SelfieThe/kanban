import './App.css'
function Comment({comment, toggleComment, removeComment}) {
    // console.log(comment)
   
    return (
        <div key={comment.id} className="list-item">            
            <div id="nameComment" onClick={() => toggleComment(comment.id)}>
                {comment.comment}                                
            </div>
            <div onClick={() => removeComment(comment.id)} className="delBtn">
                Удалить
            </div>            
        </div>
    )
}

export default Comment